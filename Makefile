OBJS = jackcpp.o

PKGS = jack

CXXFLAGS = -std=c++14 -Wall -Wextra -Werror -O2 $(shell pkg-config --cflags $(PKGS))

libjackcpp.a: $(OBJS)
	$(AR) rcs $@ $(OBJS)

jackcpp.o: jackcpp.h
