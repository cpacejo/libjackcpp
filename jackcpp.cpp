#include <array>
#include <cerrno>
#include <cstddef>
#include <cstdint>
#include <exception>
#include <functional>
#include <memory>
#include <new>
#include <string>
#include <system_error>
#include <tuple>
#include <utility>
#include <vector>
#include "jackcpp.h"

extern "C"
{
#include <jack/jack.h>
#include <jack/midiport.h>
#include <jack/ringbuffer.h>
}

namespace jack
{

namespace
{

/*void check_status(const jack_status_t status)
{
    if (status & JackFailure)
        throw failure(status);
}*/

void global_shutdown_callback(void* const arg) noexcept
{
    (*static_cast<const std::function<client::shutdown_callback>*>(arg))();
}

int global_process_callback(const jack_nframes_t nframes, void* const arg) noexcept
{
    try
    {
        (*static_cast<const std::function<client::process_callback>*>(arg))(nframes);
    }
    catch (...)
    {
        return -1;
    }

    return 0;
}

void global_latency_callback(const jack_latency_callback_mode_t mode, void* const arg) noexcept
{
    (*static_cast<const std::function<client::latency_callback>*>(arg))(
        static_cast<latency_callback_mode>(mode));
}

}


client::client(jack_client_t* const p) noexcept
    : m_p(p)
{ }

client client::open(const char* const client_name)
{
    jack_status_t status;
    const auto p = jack_client_open(client_name, JackNullOption, &status);
    if (!p) throw failure(status);
    return client(p);
}

void client::on_shutdown(std::function<shutdown_callback> callback)
{
    m_shutdown_callback = std::make_unique<std::function<shutdown_callback>>(std::move(callback));
    jack_on_shutdown(m_p.get(), global_shutdown_callback, m_shutdown_callback.get());
}

void client::set_process_callback(std::function<process_callback> callback)
{
    m_process_callback = std::make_unique<std::function<process_callback>>(std::move(callback));
    jack_set_process_callback(m_p.get(), global_process_callback, m_process_callback.get());
}

void client::set_latency_callback(std::function<latency_callback> callback)
{
    m_latency_callback = std::make_unique<std::function<latency_callback>>(std::move(callback));
    jack_set_latency_callback(m_p.get(), global_latency_callback, m_latency_callback.get());
}

port_handle client::port_register(
    const char* const port_name, const char* const port_type,
    const unsigned long flags, const unsigned long buffer_size)
{
    const auto h = jack_port_register(m_p.get(), port_name, port_type, flags, buffer_size);
    if (!h) throw port_registration_failure();
    return h;
}

void client::port_unregister(const port_handle h)
{
    if (jack_port_unregister(m_p.get(), h) != 0)
        std::terminate();
}

void client::activate()
{
    if (jack_activate(m_p.get()) != 0)
        std::terminate();
}

void client::deactivate()
{
    if (jack_deactivate(m_p.get()) != 0)
        std::terminate();
}

std::vector<std::string> client::get_ports(
    const char* const port_name_pattern, const char* const type_name_pattern,
    const unsigned long flags) const
{
    const std::unique_ptr<const char*[], deleter> ports(
        jack_get_ports(m_p.get(), port_name_pattern, type_name_pattern, flags));

    std::vector<std::string> ret;
    if (ports)
        for (const char** port_p = &ports[0]; *port_p; ++port_p)
            ret.emplace_back(*port_p);
    return ret;
}

void client::connect(const char* const source_port, const char* const destination_port)
{
    // FIXME: better error handling
    if (jack_connect(m_p.get(), source_port, destination_port) != 0)
        std::terminate();
}

void client::disconnect(const char* const source_port, const char* const destination_port)
{
    // FIXME: better error handling
    if (jack_disconnect(m_p.get(), source_port, destination_port) != 0)
        std::terminate();
}

nframes client::get_buffer_size() const
{
    return jack_get_buffer_size(m_p.get());
}

nframes client::get_sample_rate() const
{
    return jack_get_sample_rate(m_p.get());
}

std::tuple<
    nframes,  // current frames
    clock::time_point,  // current time
    clock::time_point,  // next time
    std::chrono::duration<float, std::micro>  // period
> client::get_cycle_times() const
{
    jack_nframes_t current_frames;
    jack_time_t current_usecs, next_usecs;
    float period_usecs;
    // FIXME: better error handling
    if (jack_get_cycle_times(m_p.get(), &current_frames, &current_usecs, &next_usecs, &period_usecs) != 0)
        std::terminate();

    return {
        current_frames,
        clock::time_point(clock::duration(current_usecs)),
        clock::time_point(clock::duration(next_usecs)),
        std::chrono::duration<float, std::micro>(period_usecs)
    };
}

void client::safe_close::operator()(jack_client_t* const p) noexcept
{
    if (jack_client_close(p) != 0)
        std::terminate();
}

const char* port_name(const const_port_handle h)
{
    return jack_port_name(h);
}

void port_set_alias(const port_handle h, const char* const alias)
{
    if (jack_port_set_alias(h, alias) != 0)
        throw std::length_error("jack::port_set_alias");
}

void port_unset_alias(const port_handle h, const char* const alias)
{
    // FIXME: better error handling
    if (jack_port_unset_alias(h, alias) != 0)
        std::terminate();
}

// FIXME: output type of jack_port_get_aliases is messed up... does it do what we think?
/*std::array<const char*, 2> port_get_aliases(const const_port_handle h)
{
    std::array<const char*, 2> ret;
    if (jack_port_get_aliases(h, ret.data()) != 0) std::terminate();
    return ret;
}*/

latency_range port_get_latency_range(const port_handle h, const latency_callback_mode mode)
{
    latency_range ret;
    jack_port_get_latency_range(h, static_cast<jack_latency_callback_mode_t>(mode), &ret);
    return ret;
}


//
// midiport.h
//

midi_event midi_port_buffer_handle::event_get(const std::uint32_t event_index) const
{
    midi_event event;
    const int status = jack_midi_event_get(&event, m_p, event_index);
    if (status == ENODATA) throw std::out_of_range("jack::midi_port_buffer_handle::event_get");
    if (status != 0) std::terminate();
    return event;
}

midi_data* midi_port_buffer_handle::event_reserve(const nframes time, const std::size_t data_size) const
{
    midi_data* const ptr = jack_midi_event_reserve(m_p, time, data_size);
    if (!ptr) throw std::length_error("jack::midi_port_buffer_handle::event_reserve");
    return ptr;
}

void midi_port_buffer_handle::event_write(const nframes time, const midi_data* const data, const std::size_t data_size) const
{
    const int status = jack_midi_event_write(m_p, time, data, data_size);
    if (status == ENOBUFS) throw std::length_error("jack::midi_port_buffer_handle::event_write");
    if (status != 0) std::terminate();
}


//
// ringbuffer.h
//

const_ringbuffer_handle::const_ringbuffer_handle(std::shared_ptr<const jack_ringbuffer_t> p) noexcept
    : m_p(std::move(p))
{
}

ringbuffer_handle::ringbuffer_handle(std::shared_ptr<jack_ringbuffer_t> p) noexcept
    : m_p(std::move(p))
{
}

void ringbuffer_handle::mlock() const
{
    if (jack_ringbuffer_mlock(m_p.get()) != 0)
        throw std::system_error(errno, std::generic_category(), "mlock");
}

void ringbuffer_handle::reset() const
{
    jack_ringbuffer_reset(m_p.get());
}

ringbuffer_handle::operator const_ringbuffer_handle() const&
{
    return const_ringbuffer_handle(m_p);
}

ringbuffer_handle::operator const_ringbuffer_handle() && noexcept
{
    return const_ringbuffer_handle(std::move(m_p));
}

ringbuffer_handle ringbuffer_create(const std::size_t sz)
{
    jack_ringbuffer_t* const rb = jack_ringbuffer_create(sz);
    if (!rb) throw std::bad_alloc();
    return ringbuffer_handle(std::shared_ptr<jack_ringbuffer_t>(rb, jack_ringbuffer_free));
}

}
