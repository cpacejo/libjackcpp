#ifndef JACKCPP_H
#define JACKCPP_H

#include <array>
#include <chrono>
#include <cstddef>
#include <cstdint>
#include <exception>
#include <functional>
#include <initializer_list>
#include <memory>
#include <ratio>
#include <string>
#include <tuple>
#include <vector>

extern "C"
{
#include <jack/jack.h>
#include <jack/midiport.h>
#include <jack/ringbuffer.h>
}

namespace jack
{

using nframes = jack_nframes_t;
using default_audio_sample = jack_default_audio_sample_t;

static constexpr const char* default_audio_type = JACK_DEFAULT_AUDIO_TYPE;
static constexpr const char* default_midi_type = JACK_DEFAULT_MIDI_TYPE;

class failure
{
public:
    failure(const jack_status_t status): m_status(status) { }
    jack_status_t status() const { return m_status; }

private:
    jack_status_t m_status;
};

class port_registration_failure { };

struct deleter
{
    void operator()(void* const p) { jack_free(p); }
};

using port_handle = jack_port_t*;
using const_port_handle = const jack_port_t*;

namespace port_flags
{

constexpr auto is_input = JackPortIsInput;
constexpr auto is_output = JackPortIsOutput;
constexpr auto is_physical = JackPortIsPhysical;
constexpr auto is_can_monitor = JackPortCanMonitor;
constexpr auto is_terminal = JackPortIsTerminal;

};

enum class latency_callback_mode
{
    capture_latency = JackCaptureLatency,
    playback_latency = JackPlaybackLatency
};

using latency_range = jack_latency_range_t;

class clock
{
public:
    using rep = jack_time_t;
    using period = std::micro;
    using duration = std::chrono::duration<rep, period>;
    using time_point = std::chrono::time_point<clock>;
    static constexpr bool is_steady = true;
    static time_point now() noexcept { return time_point(duration(jack_get_time())); }
};

class client
{
public:
    using shutdown_callback = void();
    using process_callback = void(nframes);
    using latency_callback = void(latency_callback_mode);

    client() noexcept = default;

    static client open(const char* client_name = nullptr);

    void on_shutdown(std::function<shutdown_callback>);
    void set_process_callback(std::function<process_callback>);
    void set_latency_callback(std::function<latency_callback>);

    port_handle port_register(const char* port_name, const char* port_type,
        unsigned long flags, unsigned long buffer_size = 0u);

    void port_unregister(port_handle);

    void activate();
    void deactivate();

    std::vector<std::string> get_ports(
        const char* port_name_pattern, const char* type_name_pattern,
        unsigned long flags) const;

    void connect(const char* source_port, const char* destination_port);
    void disconnect(const char* source_port, const char* destination_port);

    nframes get_buffer_size() const;
    nframes get_sample_rate() const;

    nframes frame_time() const noexcept { return jack_frame_time(m_p.get()); }
    nframes frames_since_cycle_start() const noexcept { return jack_frames_since_cycle_start(m_p.get()); }

    nframes last_frame_time() const noexcept { return jack_last_frame_time(m_p.get()); }
    std::tuple<
        nframes,  // current frames
        clock::time_point,  // current time
        clock::time_point,  // next time
        std::chrono::duration<float, std::micro>  // period
    > get_cycle_times() const;

    clock::time_point frames_to_time(const nframes nf) const noexcept
    { return clock::time_point(clock::duration(jack_frames_to_time(m_p.get(), nf))); }
    nframes time_to_frames(const clock::time_point tp) const noexcept
    { return jack_time_to_frames(m_p.get(), tp.time_since_epoch().count()); }

private:
    // must come before client pointer!
    std::unique_ptr<std::function<shutdown_callback>> m_shutdown_callback;
    std::unique_ptr<std::function<process_callback>> m_process_callback;
    std::unique_ptr<std::function<latency_callback>> m_latency_callback;

    struct safe_close { void operator()(jack_client_t*) noexcept; };
    std::unique_ptr<jack_client_t, safe_close> m_p;

    explicit client(jack_client_t*) noexcept;
};

// TODO: make port_handle a proper class?
const char* port_name(const_port_handle);
void port_set_alias(port_handle, const char* alias);
void port_unset_alias(port_handle, const char* alias);
//std::array<const char*, 2> port_get_aliases(const_port_handle);

template<typename T = void> T* port_get_buffer(const port_handle h, const nframes n)
{ return static_cast<T*>(jack_port_get_buffer(h, n)); }

latency_range port_get_latency_range(port_handle, latency_callback_mode);


//
// midiport.h
//

using midi_data = jack_midi_data_t;
using midi_event = jack_midi_event_t;

class midi_port_buffer_handle
{
public:
    midi_port_buffer_handle() noexcept = default;
    explicit midi_port_buffer_handle(void* const port_buffer) noexcept: m_p(port_buffer) { }

    void clear_buffer() const { jack_midi_clear_buffer(m_p); }
    nframes get_event_count() const { return jack_midi_get_event_count(m_p); }
    std::uint32_t get_lost_event_count() const { return jack_midi_get_lost_event_count(m_p); }
    midi_event event_get(std::uint32_t event_index) const;
    std::size_t max_event_size() const { return jack_midi_max_event_size(m_p); }
    midi_data* event_reserve(nframes time, std::size_t data_size) const;
    void event_write(nframes time, const midi_data* data, std::size_t data_size) const;
    void event_write(const nframes time, const std::initializer_list<midi_data> data) const
    { event_write(time, data.begin(), data.size()); }

private:
    void* m_p;
};

inline midi_port_buffer_handle port_get_midi_buffer(const port_handle h, const nframes n)
{ return midi_port_buffer_handle(port_get_buffer(h, n)); }


//
// ringbuffer.h
//

using ringbuffer = jack_ringbuffer_t;
using ringbuffer_data = jack_ringbuffer_data_t;

class const_ringbuffer_handle
{
public:
    const_ringbuffer_handle() noexcept = default;
    const_ringbuffer_handle(std::shared_ptr<const jack_ringbuffer_t>) noexcept;

    std::array<ringbuffer_data, 2> get_read_vector() const noexcept
    {
        std::array<ringbuffer_data, 2> vec;
        jack_ringbuffer_get_read_vector(m_p.get(), vec.data());
        return vec;
    }

    std::array<ringbuffer_data, 2> get_write_vector() const noexcept
    {
        std::array<ringbuffer_data, 2> vec;
        jack_ringbuffer_get_write_vector(m_p.get(), vec.data());
        return vec;
    }

    std::size_t read_space() const noexcept { return jack_ringbuffer_read_space(m_p.get()); }
    std::size_t write_space() const noexcept { return jack_ringbuffer_write_space(m_p.get()); }

private:
    std::shared_ptr<const jack_ringbuffer_t> m_p;
};

class ringbuffer_handle
{
public:
    ringbuffer_handle() noexcept = default;
    ringbuffer_handle(std::shared_ptr<jack_ringbuffer_t>) noexcept;

    std::array<ringbuffer_data, 2> get_read_vector() const noexcept
    {
        std::array<ringbuffer_data, 2> vec;
        jack_ringbuffer_get_read_vector(m_p.get(), vec.data());
        return vec;
    }

    std::array<ringbuffer_data, 2> get_write_vector() const noexcept
    {
        std::array<ringbuffer_data, 2> vec;
        jack_ringbuffer_get_write_vector(m_p.get(), vec.data());
        return vec;
    }

    std::size_t read(char* const dest, const std::size_t cnt) const noexcept { return jack_ringbuffer_read(m_p.get(), dest, cnt); }
    std::size_t peek(char* const dest, const std::size_t cnt) const noexcept { return jack_ringbuffer_peek(m_p.get(), dest, cnt); }
    void read_advance(const std::size_t cnt) const noexcept { jack_ringbuffer_read_advance(m_p.get(), cnt); }
    std::size_t read_space() const noexcept { return jack_ringbuffer_read_space(m_p.get()); }

    void mlock() const;
    void reset() const;

    std::size_t write(const char* const src, const std::size_t cnt) const noexcept { return jack_ringbuffer_write(m_p.get(), src, cnt); }
    void write_advance(const std::size_t cnt) const noexcept { jack_ringbuffer_write_advance(m_p.get(), cnt); }
    std::size_t write_space() const noexcept { return jack_ringbuffer_write_space(m_p.get()); }

    operator const_ringbuffer_handle() const&;
    operator const_ringbuffer_handle() && noexcept;

private:
    std::shared_ptr<jack_ringbuffer_t> m_p;
};

ringbuffer_handle ringbuffer_create(std::size_t);

}

#endif
